use criterion::{criterion_group, criterion_main, Criterion};
use gag::Gag;

use aoc2020::DAYS;

pub fn bench_days(c: &mut Criterion) {
    for (day, f) in DAYS.iter() {
        c.bench_function(format!("Day {}", day).as_str(), |b| {
            let _print_gag = Gag::stdout().unwrap();
            b.iter(f)
        });
    }
}

criterion_group!(benches, bench_days);
criterion_main!(benches);
