#!/usr/bin/env sh

if [ -z "$CI" ]; then
  CI_REGISTRY=registry.gitlab.com
  CI_PROJECT_ROOT_NAMESPACE=floriankisser
  CI_COMMIT_SHA=$(git rev-parse HEAD)
fi
