use std::cmp::min;
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use std::str::FromStr;

use itertools::Itertools;

type TileId = usize;
type BorderBase = u16;

#[derive(Eq, Copy, Clone, Debug)]
struct Border(BorderBase);

struct Tile {
    id: TileId,
    content: [[bool; 10]; 10],
}

impl Tile {
    fn borders(&self) -> [Border; 4] {
        let top = Border::from(&self.content[0]);
        let bottom = Border::from(&self.content[9]);
        let left = Border::from(&self.content.iter().map(|r| r[0]).collect::<Vec<_>>());
        let right = Border::from(&self.content.iter().map(|r| r[9]).collect::<Vec<_>>());
        [top, bottom, left, right]
    }
}

trait Flip {
    fn flip(&self) -> Self;
}

impl Flip for Border {
    fn flip(&self) -> Border {
        Border(self.0.reverse_bits() >> (BorderBase::BITS - 10))
    }
}

impl PartialEq for Border {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 || self.flip().0 == other.0
    }
}

impl Hash for Border {
    fn hash<H: Hasher>(&self, state: &mut H) {
        min(self.0, self.flip().0).hash(state)
    }
}

impl<T: AsRef<[bool]>> From<T> for Border {
    fn from(b: T) -> Self {
        let mut result = 0;
        b.as_ref()
            .iter()
            .enumerate()
            .filter(|(_, b)| **b)
            .for_each(|(i, _)| result |= 1 << i);
        Border(result)
    }
}

struct Image(Vec<Tile>);

impl FromStr for Image {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut result = vec![];
        for mut tile in &s.lines().chunks(12) {
            let header = tile.next().unwrap();
            let id = match header[5..9].parse::<TileId>() {
                Err(_) => return Err(format!("unexpected tile header: {}", header)),
                Ok(v) => v,
            };
            let mut content = [[false; 10]; 10];
            for (x, row) in tile.take(10).enumerate() {
                if row.len() != 10 {
                    return Err(format!("unexpected row length in tile {}", id));
                }
                for (y, c) in row.chars().enumerate() {
                    if c == '#' {
                        content[x][y] = true;
                    } else if c != '.' {
                        return Err(format!("unexpected char in tile {}: {}", id, c));
                    }
                }
            }
            result.push(Tile { id, content });
        }
        Ok(Image(result))
    }
}

impl Image {
    fn corners(&self) -> Vec<TileId> {
        let mut tile_borders = HashMap::new();
        let mut border_map: HashMap<Border, HashSet<TileId>> = HashMap::new();
        for tile in self.0.iter() {
            let borders = tile.borders();
            tile_borders.insert(tile.id, borders);
            for border in borders {
                border_map
                    .entry(border)
                    .and_modify(|s| {
                        s.insert(tile.id);
                    })
                    .or_insert_with(|| {
                        let mut s = HashSet::new();
                        s.insert(tile.id);
                        s
                    });
            }
        }
        tile_borders
            .iter()
            .filter(|(_, bs)| {
                bs.iter()
                    .filter(|b| border_map.get(b).filter(|s| s.len() > 1).is_some())
                    .count()
                    == 2
            })
            .map(|(t, _)| *t)
            .collect::<Vec<_>>()
    }
}

fn input() -> String {
    include_str!("../input/20").to_string()
}

fn calc(s: &str) -> usize {
    let image = s.parse::<Image>().unwrap();
    image.corners().iter().product()
}

pub fn run() {
    println!("Part 1: {}", calc(&input()));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(21599955909991, calc(&input()));
    }
}
