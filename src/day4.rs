use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

struct Passport {
    byr: Option<usize>,
    iyr: Option<usize>,
    eyr: Option<usize>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<usize>,
}

const EYE_COLORS: &[&str] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

impl Passport {
    pub fn is_valid(&self) -> bool {
        self.byr.is_some()
            && self.iyr.is_some()
            && self.eyr.is_some()
            && self.hgt.is_some()
            && self.hcl.is_some()
            && self.ecl.is_some()
            && self.pid.is_some()
    }

    pub fn is_valid2(&self) -> bool {
        lazy_static! {
            static ref HAIR_COLOR_RE: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
            static ref PID_RE: Regex = Regex::new(r"^[0-9]{9}$").unwrap();
        }
        self.byr.filter(|v| *v >= 1920 && *v <= 2002).is_some()
            && self.iyr.filter(|v| *v >= 2010 && *v <= 2020).is_some()
            && self.eyr.filter(|v| *v >= 2020 && *v <= 2030).is_some()
            && self
                .hgt
                .as_ref()
                .filter(|v| {
                    (v.ends_with("cm")
                        && v.trim_end_matches("cm")
                            .parse::<usize>()
                            .ok()
                            .filter(|n| *n >= 150 && *n <= 193)
                            .is_some())
                        || (v.ends_with("in")
                            && v.trim_end_matches("in")
                                .parse::<usize>()
                                .ok()
                                .filter(|n| *n >= 59 && *n <= 76)
                                .is_some())
                })
                .is_some()
            && self
                .hcl
                .as_ref()
                .filter(|v| HAIR_COLOR_RE.is_match(v))
                .is_some()
            && self
                .ecl
                .as_ref()
                .filter(|v| EYE_COLORS.contains(&&v[..]))
                .is_some()
            && self.pid.as_ref().filter(|v| PID_RE.is_match(v)).is_some()
    }

    fn parse_usize(s: &str) -> Result<usize, String> {
        match s.parse::<usize>() {
            Ok(v) => Ok(v),
            Err(_) => Err(format!("not a usize: {}", s)),
        }
    }
}

impl FromStr for Passport {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut result = Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        };
        for e in s.split_whitespace() {
            let mut split = e.splitn(2, ':');
            let field = match split.next() {
                Some(field) => field,
                None => return Err(format!("unable to parse field: {}", e)),
            };
            if field.len() != 3 {
                return Err(format!("unknown field: {}", field));
            }
            let value = match split.next() {
                Some(value) => value,
                None => return Err(format!("missing value for field: {}", field)),
            };
            match field {
                "byr" => result.byr = Some(Passport::parse_usize(value)?),
                "iyr" => result.iyr = Some(Passport::parse_usize(value)?),
                "eyr" => result.eyr = Some(Passport::parse_usize(value)?),
                "hgt" => result.hgt = Some(String::from(value)),
                "hcl" => result.hcl = Some(String::from(value)),
                "ecl" => result.ecl = Some(String::from(value)),
                "pid" => result.pid = Some(String::from(value)),
                "cid" => result.cid = Some(Passport::parse_usize(value)?),
                _ => return Err(format!("unknown field: {}", field)),
            }
        }
        Ok(result)
    }
}

fn count_valid_passports<V>(valid: V) -> usize
where
    V: Fn(&Passport) -> bool,
{
    include_str!("../input/4")
        .split("\n\n")
        .map(|s| Passport::from_str(s).unwrap())
        .filter(valid)
        .count()
}

pub fn run() {
    println!(
        "first found {} valid passports",
        count_valid_passports(Passport::is_valid)
    );
    println!(
        "then found {} valid passports",
        count_valid_passports(Passport::is_valid2)
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_valid_passports() {
        assert_eq!(254, count_valid_passports(Passport::is_valid));
    }

    #[test]
    fn test_count_valid2_passports() {
        assert_eq!(184, count_valid_passports(Passport::is_valid2));
    }
}
