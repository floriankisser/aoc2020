use std::convert::TryFrom;

enum Ast {
    Sum(Box<Ast>, Box<Ast>),
    Product(Box<Ast>, Box<Ast>),
    Number(usize),
}

#[derive(Ord, PartialOrd, Eq, PartialEq)]
enum Operator {
    LeftParentheses,
    RightParentheses,
    Add,
    Mul,
}

impl Ast {
    fn eval(&self) -> usize {
        match self {
            Ast::Sum(lhs, rhs) => lhs.eval() + rhs.eval(),
            Ast::Product(lhs, rhs) => lhs.eval() * rhs.eval(),
            Ast::Number(v) => *v,
        }
    }
}

impl TryFrom<char> for Operator {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '+' => Ok(Operator::Add),
            '*' => Ok(Operator::Mul),
            '(' => Ok(Operator::LeftParentheses),
            ')' => Ok(Operator::RightParentheses),
            _ => Err(format!("unexpected character: {}", value)),
        }
    }
}

fn parse(s: &str, with_precedence: bool) -> Result<Ast, String> {
    let mut out = vec![];
    let mut operator_stack = vec![];
    for c in s.chars().filter(|c| !c.is_whitespace()) {
        match c {
            n if n.is_numeric() => out.push(Ast::Number(n.to_digit(10).unwrap() as usize)),
            op_c if op_c == '+' || op_c == '*' => {
                let op = Operator::try_from(op_c)?;
                while let Some(op2) = operator_stack.last() {
                    if *op2 == Operator::LeftParentheses || (with_precedence && *op2 > op) {
                        break;
                    }
                    build_ast(&mut out, &mut operator_stack)?;
                }
                operator_stack.push(op);
            }
            '(' => operator_stack.push(Operator::LeftParentheses),
            ')' => {
                while !matches!(operator_stack.last(), Some(Operator::LeftParentheses)) {
                    if operator_stack.is_empty() {
                        return Err(String::from("mismatching parentheses"));
                    }
                    build_ast(&mut out, &mut operator_stack)?;
                }
                operator_stack.pop();
            }
            u => return Err(format!("unexpected character: {}", u)),
        }
    }
    while !operator_stack.is_empty() {
        build_ast(&mut out, &mut operator_stack)?;
    }
    if out.len() == 1 {
        Ok(out.pop().unwrap())
    } else {
        Err(String::from("invalid syntax"))
    }
}

fn build_ast(out: &mut Vec<Ast>, operator_stack: &mut Vec<Operator>) -> Result<(), String> {
    let (rhs, lhs) = match (out.pop(), out.pop()) {
        (Some(lhs), Some(rhs)) => (rhs, lhs),
        _ => return Err(String::from("invalid syntax")),
    };
    out.push(match operator_stack.pop() {
        Some(Operator::Add) => Ast::Sum(Box::new(lhs), Box::new(rhs)),
        Some(Operator::Mul) => Ast::Product(Box::new(lhs), Box::new(rhs)),
        Some(Operator::LeftParentheses) => return Err(String::from("mismatching parentheses")),
        _ => panic!(),
    });
    Ok(())
}

fn input() -> String {
    include_str!("../input/18").to_string()
}

fn calc(s: &str, with_precedence: bool) -> usize {
    s.lines()
        .map(|l| parse(l, with_precedence))
        .map(Result::unwrap)
        .map(|s| s.eval())
        .sum()
}

pub fn run() {
    println!("Part 1: {}", calc(&input(), false));
    println!("Part 2: {}", calc(&input(), true));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple() {
        let s = parse("1 + 2 * 3 + 4 * 5 + 6", false).unwrap();
        assert_eq!(71, s.eval());
    }

    #[test]
    fn test_parentheses1() {
        let s = parse("1 + (2 * 3) + (4 * (5 + 6))", false).unwrap();
        assert_eq!(51, s.eval());
    }

    #[test]
    fn test_parentheses2() {
        let s = parse("2 * 3 + (4 * 5)", false).unwrap();
        assert_eq!(26, s.eval());
    }

    #[test]
    fn test_parentheses3() {
        let s = parse("5 + (8 * 3 + 9 + 3 * 4 * 3)", false).unwrap();
        assert_eq!(437, s.eval());
    }

    #[test]
    fn test_parentheses4() {
        let s = parse("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", false).unwrap();
        assert_eq!(12240, s.eval());
    }

    #[test]
    fn test_parentheses5() {
        let s = parse("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", false).unwrap();
        assert_eq!(13632, s.eval());
    }

    #[test]
    fn test_part1() {
        assert_eq!(1890866893020, calc(&input(), false));
    }

    #[test]
    fn test_precedence1() {
        let s = parse("1 + 2 * 3 + 4 * 5 + 6", true).unwrap();
        assert_eq!(231, s.eval());
    }

    #[test]
    fn test_precedence2() {
        let s = parse("1 + (2 * 3) + (4 * (5 + 6))", true).unwrap();
        assert_eq!(51, s.eval());
    }

    #[test]
    fn test_precedence3() {
        let s = parse("2 * 3 + (4 * 5)", true).unwrap();
        assert_eq!(46, s.eval());
    }

    #[test]
    fn test_precedence4() {
        let s = parse("5 + (8 * 3 + 9 + 3 * 4 * 3)", true).unwrap();
        assert_eq!(1445, s.eval());
    }

    #[test]
    fn test_precedence5() {
        let s = parse("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", true).unwrap();
        assert_eq!(669060, s.eval());
    }

    #[test]
    fn test_precedence6() {
        let s = parse("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", true).unwrap();
        assert_eq!(23340, s.eval());
    }

    #[test]
    fn test_part2() {
        assert_eq!(34646237037193, calc(&input(), true));
    }
}
