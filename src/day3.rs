const PATTERN: &str = include_str!("../input/3");

fn count_slope_trees(pattern: &str, x_step: usize, y_step: usize) -> usize {
    let mut count = 0;
    let mut x_pos = 0;
    let pattern_width = pattern.lines().next().unwrap().len();
    for line in pattern.lines().step_by(y_step) {
        if line
            .chars()
            .nth(x_pos % pattern_width)
            .filter(|c| *c == '#')
            .is_some()
        {
            count += 1;
        }
        x_pos += x_step;
    }
    count
}

fn multiply_all_slope_trees() -> usize {
    [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|(x_step, y_step)| count_slope_trees(PATTERN, *x_step, *y_step))
        .product()
}

pub fn run() {
    println!(
        "found {} trees for (3, 1) slope",
        count_slope_trees(PATTERN, 3, 1)
    );
    println!("calculated {} for all slopes", multiply_all_slope_trees());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_slope_trees() {
        assert_eq!(250, count_slope_trees(PATTERN, 3, 1));
    }

    #[test]
    fn test_multiply_all_slope_trees() {
        assert_eq!(1_592_662_500, multiply_all_slope_trees());
    }
}
