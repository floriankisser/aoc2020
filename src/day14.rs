use std::collections::HashMap;
use std::convert::TryFrom;
use std::str::FromStr;

use crate::day14::MaskBit::{One, Zero, X};
use crate::day14::Statement::{Assign, Mask};

#[derive(Debug, Clone, Copy, PartialEq)]
enum MaskBit {
    Zero,
    One,
    X,
}

impl TryFrom<char> for MaskBit {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '0' => Ok(Zero),
            '1' => Ok(One),
            'X' => Ok(X),
            _ => Err(format!("unexpected mask character: {}", value)),
        }
    }
}

#[derive(Debug, PartialEq)]
enum Statement {
    Mask([MaskBit; 36]),
    Assign { address: usize, value: usize },
}

impl FromStr for Statement {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lex_iter = s.split('=');
        let lhs = match lex_iter.next() {
            None => return Err(format!("not a statement: {}", s)),
            Some(lhs) => lhs.trim(),
        };
        let rhs = match lex_iter.next() {
            None => return Err(format!("not a statement: {}", s)),
            Some(rhs) => rhs.trim(),
        };
        if lex_iter.next().is_some() {
            return Err(format!("not a statement: {}", s));
        }
        if lhs.eq("mask") {
            if rhs.chars().count() != 36 {
                return Err(format!("in statement '{}': mask does not have size 36", s));
            }
            let mut mask = [X; 36];
            for (i, bit) in rhs.chars().rev().map(MaskBit::try_from).enumerate() {
                mask[i] = match bit {
                    Err(e) => return Err(format!("in statement '{}': {}", s, e)),
                    Ok(mask) => mask,
                }
            }
            Ok(Mask(mask))
        } else if lhs.starts_with("mem") {
            let address = match lhs[4..lhs.len() - 1].parse::<usize>() {
                Ok(v) => v,
                Err(e) => return Err(format!("in statement '{}': {}", s, e)),
            };
            let value = match rhs.parse::<usize>() {
                Ok(v) => v,
                Err(e) => return Err(format!("in statement '{}': {}", s, e)),
            };
            Ok(Assign { address, value })
        } else {
            Err(format!("unexpected statement: {}", s))
        }
    }
}

fn input() -> String {
    include_str!("../input/14").to_string()
}

fn parse(s: &str) -> Vec<Statement> {
    s.lines()
        .map(Statement::from_str)
        .collect::<Result<_, _>>()
        .unwrap()
}

fn set_mask(mask: &[MaskBit]) -> usize {
    mask.iter()
        .enumerate()
        .filter_map(|(i, b)| match b {
            One => Some(1 << i),
            _ => None,
        })
        .sum()
}

fn unset_mask(mask: &[MaskBit]) -> usize {
    mask.iter()
        .enumerate()
        .filter_map(|(i, b)| match b {
            Zero => None,
            _ => Some(1 << i),
        })
        .sum()
}

fn execute1(program: &[Statement]) -> usize {
    let mut mem = HashMap::<usize, usize>::new();
    let mut current_set_mask = 0;
    let mut current_unset_mask = usize::MAX;
    for statement in program {
        match statement {
            Mask(m) => {
                current_set_mask = set_mask(m);
                current_unset_mask = unset_mask(m);
            }
            Assign { address, value } => {
                mem.insert(*address, (*value | current_set_mask) & current_unset_mask);
            }
        }
    }
    mem.values().sum()
}

fn mask_address(address: usize, mask: &[MaskBit], pos: u32) -> Vec<usize> {
    let mut new_pos = pos;
    loop {
        match mask[new_pos as usize] {
            X => {
                let high_address = address | 1 << new_pos;
                let low_address = address & !(1 << new_pos);

                new_pos += 1;
                if new_pos as usize >= mask.len() {
                    break vec![high_address, low_address];
                } else {
                    let mut result = Vec::new();
                    result.extend(mask_address(high_address, mask, new_pos));
                    result.extend(mask_address(low_address, mask, new_pos));
                    break result;
                }
            }
            _ => {
                new_pos += 1;
                if new_pos as usize >= mask.len() {
                    break vec![address];
                }
            }
        }
    }
}

fn execute2(program: &[Statement]) -> usize {
    const ZERO: [MaskBit; 36] = [Zero; 36];
    let mut mem = HashMap::<usize, usize>::new();
    let mut current_mask = &ZERO;
    for statement in program {
        match statement {
            Mask(m) => {
                current_mask = m;
            }
            Assign { address, value } => {
                mem.extend(
                    mask_address(*address | set_mask(current_mask), current_mask, 0)
                        .iter()
                        .map(|a| (*a, *value)),
                );
            }
        }
    }
    mem.values().sum()
}

pub fn run() {
    println!("first sum of memory: {}", execute1(&parse(&input())));
    println!("second sum of memory: {}", execute2(&parse(&input())));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST1: &str = r"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";

    const TEST2: &str = r"mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1";

    #[test]
    fn test_parse() {
        let p = parse(TEST1);
        let mut iter = p.iter();
        assert_eq!(
            Some(&Mask([
                X, Zero, X, X, X, X, One, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X,
                X, X, X, X, X, X, X, X, X, X
            ])),
            iter.next()
        );
        assert_eq!(
            Some(&Assign {
                address: 8,
                value: 11
            }),
            iter.next()
        );
        assert_eq!(
            Some(&Assign {
                address: 7,
                value: 101
            }),
            iter.next()
        );
        assert_eq!(
            Some(&Assign {
                address: 8,
                value: 0
            }),
            iter.next()
        );
        assert_eq!(None, iter.next());
    }

    #[test]
    fn test_part1a() {
        assert_eq!(165, execute1(&parse(TEST1)));
    }

    #[test]
    fn test_part1() {
        assert_eq!(6513443633260, execute1(&parse(&input())));
    }

    #[test]
    fn test_part2a() {
        assert_eq!(208, execute2(&parse(TEST2)));
    }

    #[test]
    fn test_part2() {
        assert_eq!(3442819875191, execute2(&parse(&input())));
    }
}
