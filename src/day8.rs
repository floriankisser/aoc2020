use std::collections::HashSet;
use std::str::FromStr;

use Execute::{Safe, Unsafe};
use Instruction::{Acc, Jmp, Nop};

#[derive(Debug, PartialEq, Clone)]
enum Instruction {
    Acc(isize),
    Jmp(isize),
    Nop(isize),
}

impl FromStr for Instruction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split_whitespace().collect();
        if split.len() != 2 {
            return Err(format!("unexpected input: {}", s));
        }
        let arg: isize = match split[1].parse() {
            Ok(arg) => arg,
            Err(e) => return Err(format!("error parsing argument: {}", e)),
        };
        match split[0] {
            "acc" => Ok(Acc(arg)),
            "jmp" => Ok(Jmp(arg)),
            "nop" => Ok(Nop(arg)),
            instruction => Err(format!("unexpected instruction: {}", instruction)),
        }
    }
}

fn load(source: &str) -> Vec<Instruction> {
    source
        .lines()
        .map(Instruction::from_str)
        .map(Result::unwrap)
        .collect()
}

fn input() -> String {
    include_str!("../input/8").to_string()
}

enum Execute {
    Safe,
    Unsafe,
}

fn execute(program: &[Instruction], o: Execute) -> Result<isize, String> {
    let mut p: usize = 0;
    let mut a: isize = 0;
    let mut v: HashSet<usize> = HashSet::new();

    while p < program.len() {
        if v.contains(&p) {
            return match o {
                Execute::Safe => Err(String::from("loop detected")),
                Execute::Unsafe => Ok(a),
            };
        }
        v.insert(p);
        match program[p] {
            Acc(arg) => {
                a += arg;
                p += 1;
            }
            Jmp(arg) => p = p.wrapping_add(arg as usize),
            Nop(_) => p += 1,
        }
    }
    Ok(a)
}

fn fix(mut program: Vec<Instruction>) -> isize {
    let mut p: usize = 0;
    while p < program.len() {
        let backup = program[p].clone();
        let m: Instruction = match program[p] {
            Acc(_) => {
                p += 1;
                continue;
            }
            Jmp(arg) => Nop(arg),
            Nop(arg) => Jmp(arg),
        };
        program[p] = m;
        match execute(&program, Safe) {
            Ok(v) => return v,
            Err(_) => {
                program[p] = backup;
                p += 1
            }
        }
    }
    panic!();
}

pub fn run() {
    let program = load(&input());
    println!("accumulator: {}", execute(&program, Unsafe).unwrap());
    println!("accumulator for fixed program: {}", fix(program));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_acc() {
        assert_eq!(Acc(-99), "acc -99".parse().unwrap());
    }

    #[test]
    fn test_jmp() {
        assert_eq!(Jmp(1), "jmp +1".parse().unwrap());
    }

    #[test]
    fn test_nop() {
        assert_eq!(Nop(100), "nop +100".parse().unwrap());
    }

    #[test]
    fn test_load() {
        assert_eq!(647, load(&input()).len())
    }

    #[test]
    fn test_program1() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        assert_eq!(5, execute(&load(input), Unsafe).unwrap());
    }

    #[test]
    fn test_part1() {
        assert_eq!(1949, execute(&load(&input()), Unsafe).unwrap());
    }

    #[test]
    fn test_part2() {
        assert_eq!(2092, fix(load(&input())));
    }
}
