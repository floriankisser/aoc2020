use std::str::FromStr;

struct Input {
    earliest: usize,
    buses: Vec<Option<usize>>,
}

impl FromStr for Input {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        let earliest = match lines.next() {
            None => return Err(String::from("unexpected end of file: no earliest")),
            Some(v) => match v.parse::<usize>() {
                Ok(e) => e,
                Err(e) => return Err(format!("error parsing earliest: {}", e)),
            },
        };
        let buses = match lines.next() {
            None => return Err(String::from("unexpected end of file: no busses")),
            Some(v) => v
                .split(',')
                .map(|e| {
                    if e == "x" {
                        Ok(None)
                    } else {
                        match e.parse() {
                            Ok(b) => Ok(Some(b)),
                            Err(e) => Err(e),
                        }
                    }
                })
                .collect(),
        };
        let buses = match buses {
            Ok(v) => v,
            Err(e) => return Err(format!("error parsing bus: {}", e)),
        };
        Ok(Input { earliest, buses })
    }
}

fn input() -> String {
    include_str!("../input/13").to_string()
}

fn calc1(input: &Input) -> usize {
    input
        .buses
        .iter()
        .filter_map(|&e| e)
        .map(|b| (b, b - (input.earliest % b)))
        .min_by_key(|e| e.1)
        .map(|r| r.0 * r.1)
        .unwrap()
}

fn calc2(input: &Input) -> usize {
    let buses: Vec<(usize, usize)> = input
        .buses
        .iter()
        .enumerate()
        .filter(|(_, &e)| e.is_some())
        .map(|(n, &e)| (n, e.unwrap()))
        .collect();
    let mut t = 0;
    let mut step = buses[0].1;
    for (n, bus) in &buses[1..] {
        while (t + n) % bus != 0 {
            t += step;
        }
        step *= bus;
    }
    t
}

pub fn run() {
    let input = input().parse().unwrap();
    println!("Part 1: {}", calc1(&input));
    println!("Part 2: {}", calc2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = r"939
7,13,x,x,59,x,31,19";

    #[test]
    fn test_parse() {
        let input = TEST.parse::<Input>().unwrap();
        assert_eq!(939, input.earliest);
        assert_eq!(8, input.buses.len());
    }

    #[test]
    fn test_part1a() {
        assert_eq!(295, calc1(&TEST.parse().unwrap()));
    }

    #[test]
    fn test_part1() {
        assert_eq!(171, calc1(&input().parse().unwrap()));
    }

    #[test]
    fn test_part2a() {
        assert_eq!(1068781, calc2(&TEST.parse().unwrap()));
    }

    #[test]
    fn test_part2b() {
        assert_eq!(3417, calc2(&"0\n17,x,13,19".parse().unwrap()));
    }

    #[test]
    fn test_part2() {
        assert_eq!(539746751134958, calc2(&input().parse().unwrap()));
    }
}
