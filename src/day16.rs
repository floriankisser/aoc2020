use std::collections::HashMap;
use std::ops::{Deref, RangeInclusive};
use std::str::FromStr;

#[derive(Debug, PartialEq)]
struct Rule {
    field: String,
    ranges: Vec<RangeInclusive<usize>>,
}

impl Rule {
    pub fn valid(&self, value: &usize) -> bool {
        self.ranges.iter().any(|r| r.contains(value))
    }
}

impl FromStr for Rule {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.splitn(2, ':');
        let field = match iter.next() {
            None => return Err(format!("missing field name for rule: {}", s)),
            Some(v) => v.to_string(),
        };
        let ranges = match iter.next() {
            None => return Err(format!("missing ranges for rule: {}", s)),
            Some(v) => v
                .split(" or ")
                .map(|r| {
                    let bounds = r
                        .trim()
                        .splitn(2, '-')
                        .map(|b| match b.parse() {
                            Ok(v) => Ok(v),
                            Err(e) => Err(format!("invalid range: {}: {}", v, e)),
                        })
                        .collect::<Result<Vec<_>, String>>()?;
                    if bounds.len() == 2 {
                        Ok(RangeInclusive::new(
                            *bounds.get(0).unwrap(),
                            *bounds.get(1).unwrap(),
                        ))
                    } else {
                        Err(format!("invalid range: {}", r))
                    }
                })
                .collect::<Result<_, String>>()?,
        };
        Ok(Rule { field, ranges })
    }
}

#[derive(Debug, PartialEq)]
struct Ticket(Vec<usize>);

impl Deref for Ticket {
    type Target = Vec<usize>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl FromStr for Ticket {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.split(',').map(usize::from_str).collect() {
            Ok(values) => Ok(Ticket(values)),
            Err(e) => Err(format!("{}", e)),
        }
    }
}

struct Notes {
    rules: Vec<Rule>,
    my_ticket: Ticket,
    other_tickets: Vec<Ticket>,
}

impl FromStr for Notes {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rules = Vec::new();
        let mut my_ticket = None;
        let mut other_tickets = Vec::new();

        let mut lines = s.lines();
        while let Some(line) = lines.next() {
            let line = line.trim();
            if line.is_empty() {
                continue;
            }
            if line == "your ticket:" {
                my_ticket = match lines.next() {
                    None => return Err(String::from("missing my ticket content")),
                    Some(ticket) => Some(ticket.parse()?),
                }
            } else if line == "nearby tickets:" {
                break;
            } else {
                rules.push(line.parse()?);
            }
        }
        for line in lines {
            other_tickets.push(line.parse()?);
        }

        if my_ticket.is_none() {
            return Err(String::from("missing my ticket"));
        }

        Ok(Notes {
            rules,
            my_ticket: my_ticket.unwrap(),
            other_tickets,
        })
    }
}

fn input() -> String {
    include_str!("../input/16").to_string()
}

fn filter_valid(ticket: &Ticket, rules: &[Rule]) -> bool {
    ticket.iter().all(|v| rules.iter().any(|r| r.valid(v)))
}

fn calc1(notes: &Notes) -> usize {
    notes
        .other_tickets
        .iter()
        .filter(|t| !filter_valid(t, &notes.rules))
        .flat_map(|t| t.iter())
        .filter(|&v| notes.rules.iter().all(|r| !r.valid(v)))
        .sum()
}

fn map(notes: &Notes) -> HashMap<String, usize> {
    let valid_tickets = notes
        .other_tickets
        .iter()
        .filter(|t| filter_valid(t, &notes.rules))
        .collect::<Vec<_>>();
    let field_count = notes.rules.len();
    let candidates: HashMap<String, &Rule> =
        notes.rules.iter().map(|r| (r.field.clone(), r)).collect();
    let mut candidates = (0..field_count)
        .map(|_| candidates.clone())
        .collect::<Vec<_>>();
    for i in 0..field_count {
        for ticket in &valid_tickets {
            candidates[i].retain(|_, r| r.valid(&ticket[i]))
        }
        if candidates[i].is_empty() {
            panic!("no candidates left for field number {}", i);
        }
        if candidates[i].len() == 1 {
            let mut to_remove = vec![candidates[i].keys().next().cloned().unwrap()];
            loop {
                let mut new_to_remove = Vec::new();
                for field_candidates in &mut candidates {
                    if field_candidates.len() > 1 {
                        for single_remove in &to_remove {
                            field_candidates.remove(single_remove);
                        }
                        if field_candidates.len() == 1 {
                            let field = field_candidates.keys().next().cloned().unwrap();
                            to_remove.push(field.clone());
                            new_to_remove.push(field);
                        }
                    }
                }
                if new_to_remove.is_empty() {
                    break;
                }
                to_remove = new_to_remove;
            }
        }
    }
    let candidates = candidates;
    let mut field_order = Vec::with_capacity(field_count);
    for field_candidates in candidates {
        if field_candidates.len() != 1 {
            panic!(
                "expected one candidate left, got {}",
                field_candidates.len()
            );
        }
        field_order.push(field_candidates.keys().next().cloned().unwrap());
    }
    field_order
        .iter()
        .cloned()
        .zip(notes.my_ticket.iter().copied())
        .collect()
}

fn calc2(ticket: &HashMap<String, usize>) -> usize {
    ticket
        .iter()
        .filter(|(k, _)| k.starts_with("departure"))
        .map(|(_, v)| *v)
        .product()
}

pub fn run() {
    let notes = input().parse::<Notes>().unwrap();
    println!("Part 1: {}", calc1(&notes));
    println!("Part 2: {}", calc2(&map(&notes)));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST1: &str = r"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    const TEST2: &str = r"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";

    #[test]
    fn test_parse() {
        let notes = TEST1.parse::<Notes>().unwrap();
        assert_eq!(3, notes.rules.len());
        assert_eq!(Ticket(vec![7, 1, 14]), notes.my_ticket);
        assert_eq!(4, notes.other_tickets.len());
    }

    #[test]
    fn test_part1a() {
        assert_eq!(71, calc1(&TEST1.parse().unwrap()));
    }

    #[test]
    fn test_part1() {
        assert_eq!(19240, calc1(&input().parse().unwrap()));
    }

    #[test]
    fn test_part2a() {
        let ticket = map(&TEST2.parse().unwrap());
        assert_eq!(Some(&12), ticket.get("class"));
        assert_eq!(Some(&11), ticket.get("row"));
        assert_eq!(Some(&13), ticket.get("seat"));
    }

    #[test]
    fn test_part2() {
        assert_eq!(21095351239483, calc2(&map(&input().parse().unwrap())));
    }
}
