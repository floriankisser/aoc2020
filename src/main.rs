use clap::{App, AppSettings, Arg, SubCommand};

use aoc2020::DAYS;

fn main() {
    let matches = App::new("Advent of Code")
        .version("2020")
        .author("Florian Kisser")
        .subcommand(
            SubCommand::with_name("day").arg(
                Arg::with_name("day")
                    .value_name("DAY")
                    .required(true)
                    .index(1),
            ),
        )
        .subcommand(SubCommand::with_name("all"))
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("day") {
        let day = matches.value_of("day").unwrap();
        if let Some(method) = DAYS.iter().filter(|(d, _)| d == day).map(|(_, f)| f).next() {
            method();
        } else {
            eprintln!("unknown day: {}", day);
            std::process::exit(1);
        }
    }

    if matches.subcommand_matches("all").is_some() {
        DAYS.iter().for_each(|(d, f)| {
            println!("Day {}:", d);
            f();
            println!();
        });
    }
}

#[cfg(test)]
mod tests {
    use assert_cmd::Command;

    #[test]
    fn test_all() {
        let mut cmd = Command::cargo_bin("aoc2020").unwrap();
        cmd.arg("all").assert().success();
    }
}
