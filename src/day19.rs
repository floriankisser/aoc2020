use std::collections::HashSet;
use std::mem::replace;
use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, PartialEq)]
enum Rule {
    Concatenation(Vec<usize>),
    Alternation(Vec<Vec<usize>>),
    Sequence(String),
}

impl FromStr for Rule {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref CONCATENATION_RE: Regex = Regex::new(r"^(\d+ )*\d+$").unwrap();
            static ref ALTERNATION_RE: Regex = Regex::new(r"^(\d+) (\d+) \| (\d+) (\d+)$").unwrap();
            static ref SEQUENCE_RE: Regex = Regex::new(r#"^"(.+)"$"#).unwrap();
        }
        if s.contains('|') {
            let mut alternatives = vec![];
            for c in s.split('|').map(&str::trim) {
                if !CONCATENATION_RE.is_match(c) {
                    return Err(String::from("unknown rule syntax"));
                }
                alternatives.push(
                    c.split_whitespace()
                        .map(|n| n.parse::<usize>().unwrap())
                        .collect(),
                )
            }
            Ok(Rule::Alternation(alternatives))
        } else if CONCATENATION_RE.is_match(s) {
            Ok(Rule::Concatenation(
                s.split_whitespace()
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect(),
            ))
        } else if let Some(c) = SEQUENCE_RE.captures(s) {
            Ok(Rule::Sequence(String::from(&c[1])))
        } else {
            Err(String::from("unknown rule syntax"))
        }
    }
}

struct Input {
    rules: Vec<Rule>,
    values: Vec<String>,
}

impl FromStr for Input {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut rules: Vec<Option<Rule>> = vec![];
        let mut values = vec![];
        let mut lines = s.lines();
        for line in &mut lines {
            if line.is_empty() {
                break;
            }
            match line.split_once(":") {
                None => return Err(String::from("expected rule")),
                Some((n, r)) => {
                    let number = match n.parse::<usize>() {
                        Ok(n) => n,
                        Err(e) => return Err(format!("expected rule number: {}", e)),
                    };
                    let rule = match r.trim().parse() {
                        Ok(r) => r,
                        Err(msg) => return Err(msg),
                    };
                    if number >= rules.len() {
                        rules.resize_with(number + 1, Default::default)
                    }
                    if replace(&mut rules[number], Some(rule)).is_some() {
                        return Err(format!("duplicate rule number: {}", number));
                    }
                }
            }
        }
        for line in lines {
            values.push(line.to_string());
        }
        Ok(Input {
            rules: rules
                .into_iter()
                .enumerate()
                .map(|(n, r)| r.ok_or_else(|| format!("missing rule {}", n)))
                .collect::<Result<_, _>>()?,
            values,
        })
    }
}

impl Input {
    #[allow(dead_code)]
    fn count_valid_exploding(&self) -> usize {
        let valid = explode(&self.rules, &self.rules[0]);
        self.values.iter().filter(|&s| valid.contains(s)).count()
    }

    fn count_valid_matching(&self) -> usize {
        self.values
            .iter()
            .filter(|&s| self.matches(&[0], s))
            .count()
    }

    fn matches(&self, rules: &[usize], msg: &str) -> bool {
        if rules.is_empty() {
            return msg.is_empty();
        }
        let head = rules[0];
        let tail = &rules[1..];
        match self.rules.get(head).unwrap() {
            Rule::Concatenation(l) => self.matches(&concat(l, tail), msg),
            Rule::Alternation(o) => o.iter().any(|l| self.matches(&concat(l, tail), msg)),
            Rule::Sequence(s) => msg.starts_with(s) && self.matches(tail, &msg[s.len()..]),
        }
    }
}

fn concat<T: Copy>(l1: &[T], l2: &[T]) -> Vec<T> {
    l1.iter().chain(l2).copied().collect()
}

fn explode(rules: &[Rule], current: &Rule) -> HashSet<String> {
    match current {
        Rule::Concatenation(c) => {
            let mut iter = c.iter();
            let mut result = explode(rules, &rules[*iter.next().unwrap()]);
            for e in iter {
                let sub = explode(rules, &rules[*e]);
                result = result
                    .iter()
                    .flat_map(|s1| sub.iter().map(move |s2| s1.clone() + s2))
                    .collect()
            }
            result
        }
        Rule::Alternation(a) => a
            .iter()
            .flat_map(|c| {
                let rule = Rule::Concatenation(c.to_vec());
                explode(rules, &rule).into_iter()
            })
            .collect(),
        Rule::Sequence(s) => {
            let mut result = HashSet::with_capacity(1);
            result.insert(s.clone());
            result
        }
    }
}

fn input() -> String {
    include_str!("../input/19").to_string()
}

fn change_part2(input: &mut Input) {
    input.rules[8] = Rule::Alternation(vec![vec![42], vec![42, 8]]);
    input.rules[11] = Rule::Alternation(vec![vec![42, 31], vec![42, 11, 31]]);
}

pub fn run() {
    let mut input = input().parse::<Input>().unwrap();
    println!("Part 1: {}", input.count_valid_matching());
    change_part2(&mut input);
    println!("Part 2: {}", input.count_valid_matching());
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = r#"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"#;

    #[test]
    fn test_parse() {
        let input = TEST.parse::<Input>().unwrap();
        let mut rules = input.rules.iter();
        assert_eq!(rules.next(), Some(&Rule::Concatenation(vec![4, 1, 5])));
        assert_eq!(
            rules.next(),
            Some(&Rule::Alternation(vec![vec![2, 3], vec![3, 2]]))
        );
        assert_eq!(
            rules.next(),
            Some(&Rule::Alternation(vec![vec![4, 4], vec![5, 5]]))
        );
        assert_eq!(
            rules.next(),
            Some(&Rule::Alternation(vec![vec![4, 5], vec![5, 4]]))
        );
        assert_eq!(rules.next(), Some(&Rule::Sequence(String::from("a"))));
        assert_eq!(rules.next(), Some(&Rule::Sequence(String::from("b"))));
        assert_eq!(rules.next(), None);
        assert_eq!(5, input.values.len());
    }

    #[test]
    fn test_explode() {
        let input = TEST.parse::<Input>().unwrap();
        let result = explode(&input.rules, input.rules.get(0).unwrap());
        assert_eq!(8, result.len());
        assert!(result.contains("aaaabb"));
        assert!(result.contains("aaabab"));
        assert!(result.contains("abbabb"));
        assert!(result.contains("abbbab"));
        assert!(result.contains("aabaab"));
        assert!(result.contains("aabbbb"));
        assert!(result.contains("abaaab"));
        assert!(result.contains("ababbb"));
    }

    #[test]
    fn test_part1_exploding() {
        assert_eq!(
            173,
            input().parse::<Input>().unwrap().count_valid_exploding()
        );
    }

    #[test]
    fn test_part1_matching() {
        assert_eq!(
            173,
            input().parse::<Input>().unwrap().count_valid_matching()
        );
    }

    #[test]
    fn test_part2() {
        let mut input = input().parse::<Input>().unwrap();
        change_part2(&mut input);
        assert_eq!(367, input.count_valid_matching());
    }
}
