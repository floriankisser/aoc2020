use std::convert::TryFrom;

use Action::{Leave, Occupy};
use Field::{Floor, Seat};

#[derive(Debug, Copy, Clone, PartialEq)]
enum Field {
    Floor,
    Seat { occupied: bool },
}

enum Action {
    Occupy,
    Leave,
}

impl TryFrom<char> for Field {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Floor),
            'L' => Ok(Seat { occupied: false }),
            '#' => Ok(Seat { occupied: true }),
            _ => Err(format!("unexpected character: {}", value)),
        }
    }
}

fn parse(input: &str) -> Vec<Vec<Field>> {
    input
        .lines()
        .map(|l| l.chars().map(Field::try_from).map(Result::unwrap).collect())
        .collect()
}

fn input() -> String {
    include_str!("../input/11").to_string()
}

fn relocate<F>(s: &[Vec<Field>], f: F) -> Option<Vec<Vec<Field>>>
where
    F: Fn(&[Vec<Field>], usize, usize) -> Option<Action>,
{
    let mut relocated = false;
    let mut result = Vec::with_capacity(s.len());
    for (x, row) in s.iter().enumerate() {
        let mut new_row: Vec<Field> = Vec::with_capacity(row.len());
        for (y, field) in row.iter().enumerate() {
            match field {
                Floor => new_row.push(Floor),
                Seat { .. } => {
                    let new_field;
                    if let Some(action) = f(s, x, y) {
                        relocated = true;
                        match action {
                            Occupy => new_field = Seat { occupied: true },
                            Leave => new_field = Seat { occupied: false },
                        }
                    } else {
                        new_field = *field;
                    }
                    new_row.push(new_field);
                }
            }
        }
        result.push(new_row);
    }
    if relocated {
        Some(result)
    } else {
        None
    }
}

fn action1(s: &[Vec<Field>], x: usize, y: usize) -> Option<Action> {
    let current_occupied = occupied(s, x, y).unwrap();
    let x_max = s.len() - 1;
    let y_max = s.get(0).unwrap().len() - 1;
    let x_search = x.saturating_sub(1)..=x_max.min(x + 1);
    let y_search = y.saturating_sub(1)..=y_max.min(y + 1);
    let mut occupied_count = 0;
    for x_look in x_search {
        for y_look in y_search.clone() {
            if x_look == x && y_look == y {
                continue;
            } else if let Seat { occupied: true } = s[x_look][y_look] {
                occupied_count += 1;
            }
        }
    }
    let occupied_count = occupied_count;
    if !current_occupied && occupied_count == 0 {
        Some(Occupy)
    } else if current_occupied && occupied_count >= 4 {
        Some(Leave)
    } else {
        None
    }
}

fn action2(s: &[Vec<Field>], x: usize, y: usize) -> Option<Action> {
    static DIRECTIONS: [(i8, i8); 8] = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (1, -1),
        (1, 0),
        (1, 1),
        (0, -1),
        (0, 1),
    ];
    let current_occupied = occupied(s, x, y).unwrap();
    let occupied_count = DIRECTIONS
        .iter()
        .filter(|(x_path, y_path)| neighbor_occupied(s, x, y, *x_path, *y_path))
        .count();
    if !current_occupied && occupied_count == 0 {
        Some(Occupy)
    } else if current_occupied && occupied_count >= 5 {
        Some(Leave)
    } else {
        None
    }
}

fn occupied(s: &[Vec<Field>], x: usize, y: usize) -> Result<bool, String> {
    match s[x].get(y) {
        Some(field) => match field {
            Field::Seat { occupied } => Ok(*occupied),
            Floor => Err(String::from("position is not a seat")),
        },
        None => Err(String::from("position is out of bounds")),
    }
}

fn neighbor_occupied(s: &[Vec<Field>], x: usize, y: usize, x_path: i8, y_path: i8) -> bool {
    let x_max = s.len() - 1;
    let y_max = s.get(0).unwrap().len() - 1;
    let x_test = walk_axis(x, x_max, x_path);
    let y_test = walk_axis(y, y_max, y_path);
    if (x_path != 0 && x == x_test) || (y_path != 0 && y == y_test) {
        false
    } else {
        neighbor_occupied_recursive(s, x_test, y_test, x_path, y_path)
    }
}

fn neighbor_occupied_recursive(
    s: &[Vec<Field>],
    x: usize,
    y: usize,
    x_path: i8,
    y_path: i8,
) -> bool {
    let x_max = s.len() - 1;
    let y_max = s.get(0).unwrap().len() - 1;
    match s[x].get(y).unwrap() {
        Field::Seat { occupied } => *occupied,
        Floor => {
            let x_test = walk_axis(x, x_max, x_path);
            let y_test = walk_axis(y, y_max, y_path);
            if (x_path != 0 && x == x_test) || (y_path != 0 && y == y_test) {
                false
            } else {
                neighbor_occupied_recursive(s, x_test, y_test, x_path, y_path)
            }
        }
    }
}

fn walk_axis(v: usize, max: usize, d: i8) -> usize {
    if d == 0 {
        v
    } else if d.is_positive() {
        match v.checked_add(d as usize) {
            None => v,
            Some(result) => {
                if result > max {
                    v
                } else {
                    result
                }
            }
        }
    } else {
        match v.checked_sub(d.wrapping_abs() as usize) {
            None => v,
            Some(result) => result,
        }
    }
}

fn finally_occupied<F>(s: &[Vec<Field>], f: F) -> usize
where
    F: Fn(&[Vec<Field>], usize, usize) -> Option<Action>,
{
    let mut current = match relocate(s, &f) {
        None => return count_occupied(s),
        Some(result) => result,
    };
    while let Some(next) = relocate(&current, &f) {
        current = next;
    }
    count_occupied(&current)
}

fn count_occupied(s: &[Vec<Field>]) -> usize {
    let mut result = 0;
    for row in s {
        for f in row {
            if let Seat { occupied: true } = f {
                result += 1;
            }
        }
    }
    result
}

pub fn run() {
    let input = parse(&input());
    println!(
        "number of occupied seats for first variant: {}",
        finally_occupied(&input, action1),
    );
    println!(
        "number of occupied seats for second variant: {}",
        finally_occupied(&input, action2),
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    const STATE_1: &str = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";

    const STATE_2: &str = r"#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##";

    const STATE_3: &str = r"#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#";

    #[test]
    fn test_part1() {
        assert_eq!(2249, finally_occupied(&parse(&input()), action1));
    }

    #[test]
    fn test_part2a() {
        assert_eq!(26, finally_occupied(&parse(STATE_1), action2));
    }

    #[test]
    fn test_walk_axis_negative() {
        assert_eq!(1, walk_axis(2, 9, -1));
    }

    #[test]
    fn test_walk_axis_underflow() {
        assert_eq!(0, walk_axis(0, 9, -1));
    }

    #[test]
    fn test_walk_axis_overflow() {
        assert_eq!(9, walk_axis(9, 9, 1));
    }

    #[test]
    fn test_action2a() {
        let relocation = relocate(&parse(STATE_1), action2);
        assert!(relocation.is_some());
        assert_eq!(parse(STATE_2), relocation.unwrap());
    }

    #[test]
    fn test_action2b() {
        let relocation = relocate(&parse(STATE_2), action2);
        assert!(relocation.is_some());
        assert_eq!(parse(STATE_3), relocation.unwrap());
    }

    #[test]
    fn test_part2() {
        assert_eq!(2023, finally_occupied(&parse(&input()), action2));
    }
}
