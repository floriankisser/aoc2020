fn input() -> String {
    include_str!("../input/9").to_string()
}

fn parse(s: &str) -> Vec<usize> {
    s.lines().map(|l| l.parse().unwrap()).collect()
}

fn check_number(n: usize, p: &[usize]) -> bool {
    let len = p.len();
    p.iter()
        .enumerate()
        .any(|(i, &e1)| p[i + 1..len].iter().any(|&e2| e1 + e2 == n))
}

fn find_violation(i: &[usize]) -> Option<usize> {
    let len = i.len();
    if len < 25 {
        panic!("input too small");
    }
    for p in 25..len {
        let n = i[p];
        let v = &i[p - 25..p];
        if !check_number(n, v) {
            return Some(n);
        }
    }
    None
}

fn find_summands(i: &[usize], sum: usize) -> Option<&[usize]> {
    let sum = sum as isize;
    let mut current_sum = i[0] as isize;
    let mut first = 0;
    let mut last = 0;
    loop {
        while current_sum < sum {
            last += 1;
            if last >= i.len() {
                return None;
            }
            current_sum = current_sum.wrapping_add(i[last] as isize);
        }
        if current_sum == sum {
            return Some(&i[first..=last]);
        } else {
            current_sum = current_sum.wrapping_sub(i[last] as isize);
            last -= 1;
            current_sum = current_sum.wrapping_sub(i[first] as isize);
            first += 1;
        }
    }
}

pub fn run() {
    let i = parse(&input());
    let n = find_violation(&i).unwrap();
    println!("first violating number: {}", n);
    let s = find_summands(&i, n).unwrap();
    let min = s.iter().min().unwrap();
    let max = s.iter().max().unwrap();
    println!(
        "sum of min ({}) and max ({}) values in summands is {}",
        min,
        max,
        min + max,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(133_015_568, find_violation(&parse(&input())).unwrap());
    }

    #[test]
    fn test_part2() {
        let input = parse(&input());
        let part1 = find_violation(&input).unwrap();
        let part2 = find_summands(&input, part1).unwrap();
        assert_eq!(
            16_107_959,
            part2.iter().min().unwrap() + part2.iter().max().unwrap()
        );
    }

    #[test]
    fn test_find_summands() {
        let input = parse(
            "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576",
        );
        let summands = find_summands(&input, 127).unwrap();
        assert_eq!([15, 25, 47, 40], summands);
    }
}
