use std::convert::TryFrom;
use std::ops;
use std::str::FromStr;

use crate::day12::Action::{Forward, Left, Move, Right};
use crate::day12::Direction::{East, North, South, West};

#[derive(Debug, PartialEq)]
enum Direction {
    North,
    South,
    East,
    West,
}

#[derive(Debug, PartialEq)]
enum Action {
    Move(Direction, usize),
    Left(u16),
    Right(u16),
    Forward(usize),
}

impl FromStr for Action {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let value = match usize::from_str(&s[1..]) {
            Ok(v) => v,
            Err(e) => return Err(format!("error parsing value: {}", e)),
        };
        match s.bytes().next().unwrap() as char {
            'N' => Ok(Move(North, value)),
            'S' => Ok(Move(South, value)),
            'E' => Ok(Move(East, value)),
            'W' => Ok(Move(West, value)),
            'L' => Ok(Left((value % 360) as u16)),
            'R' => Ok(Right((value % 360) as u16)),
            'F' => Ok(Forward(value)),
            c => Err(format!("invalid action: {}", c)),
        }
    }
}

fn input() -> String {
    include_str!("../input/12").to_string()
}

fn parse(s: &str) -> Vec<Action> {
    s.lines()
        .map(Action::from_str)
        .map(Result::unwrap)
        .collect()
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Position {
    x: isize,
    y: isize,
}

impl Position {
    pub fn manhattan(&self) -> usize {
        self.x.abs() as usize + self.y.abs() as usize
    }

    pub fn do_move(&self, d: &Direction, v: usize) -> Position {
        match d {
            North => Position {
                x: self.x,
                y: self.y + v as isize,
            },
            South => Position {
                x: self.x,
                y: self.y - v as isize,
            },
            East => Position {
                x: self.x + v as isize,
                y: self.y,
            },
            West => Position {
                x: self.x - v as isize,
                y: self.y,
            },
        }
    }

    pub fn rotate(&self, d: isize) -> Position {
        let effective = d.rem_euclid(360);
        if effective == 90 {
            self.rotate_left()
        } else if effective == 180 {
            *self * -1
        } else if effective == 270 {
            self.rotate_right()
        } else {
            panic!("can only rotate by right angles")
        }
    }

    pub fn rotate_right(&self) -> Position {
        Position {
            x: self.y,
            y: -self.x,
        }
    }

    pub fn rotate_left(&self) -> Position {
        Position {
            x: -self.y,
            y: self.x,
        }
    }
}

impl ops::Add for Position {
    type Output = Self;

    fn add(self, other: Position) -> Self {
        Position {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Mul<isize> for Position {
    type Output = Self;

    fn mul(self, rhs: isize) -> Self {
        Position {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

fn follow1(route: &[Action]) -> usize {
    const COMPASS: [Direction; 4] = [East, North, West, South];

    let mut position = Position { x: 0, y: 0 };
    let mut needle: isize = 0;

    for action in route {
        match action {
            Move(d, v) => position = position.do_move(d, *v),
            Left(d) => needle = (needle + (d / 90) as isize).rem_euclid(4),
            Right(d) => needle = (needle - (d / 90) as isize).rem_euclid(4),
            Forward(v) => {
                position = position.do_move(&COMPASS[usize::try_from(needle).unwrap()], *v)
            }
        }
    }

    position.manhattan()
}

fn follow2(route: &[Action]) -> usize {
    let mut position = Position { x: 0, y: 0 };
    let mut waypoint = Position { x: 10, y: 1 };

    for action in route {
        match action {
            Move(d, v) => waypoint = waypoint.do_move(d, *v),
            Left(d) => waypoint = waypoint.rotate(*d as isize),
            Right(d) => waypoint = waypoint.rotate(-(*d as isize)),
            Forward(v) => {
                position = position + waypoint * isize::try_from(*v).unwrap();
            }
        }
    }
    position.manhattan()
}

pub fn run() {
    let route = parse(&input());
    println!("part 1 manhattan distance: {}", follow1(&route));
    println!("part 2 manhattan distance: {}", follow2(&route));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = r"F10
N3
F7
R90
F11";

    #[test]
    fn test_parse_action_north() {
        assert_eq!(Move(North, 3), "N3".parse().unwrap());
    }

    #[test]
    fn test_parse_action_left() {
        assert_eq!(Left(90), "L90".parse().unwrap());
    }

    #[test]
    fn test_part1a() {
        assert_eq!(25, follow1(&parse(TEST)));
    }

    #[test]
    fn test_part1() {
        assert_eq!(2297, follow1(&parse(&input())));
    }

    #[test]
    fn test_part2a() {
        assert_eq!(286, follow2(&parse(TEST)));
    }

    #[test]
    fn test_part2() {
        assert_eq!(89984, follow2(&parse(&input())));
    }
}
