use std::cmp::{max, min};
use std::collections::HashSet;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::{Display, Formatter};
use std::hash::Hash;
use std::str::FromStr;

trait Coord {
    fn neighbors(&self) -> Vec<Self>
    where
        Self: Sized;

    fn min(rhs: &Self, lhs: &Self) -> Self;

    fn max(rhs: &Self, lhs: &Self) -> Self;
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Coord3 {
    x: isize,
    y: isize,
    z: isize,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Coord4 {
    x: isize,
    y: isize,
    z: isize,
    w: isize,
}

enum State {
    Inactive,
    Active,
}

struct Active<C: Coord + Eq + Hash>(HashSet<C>);

struct Room<C: Coord> {
    min: C,
    max: C,
}

#[derive(Debug)]
enum RoomError {
    EmptyRoom,
}

impl<C: Coord + Copy> From<&C> for Room<C> {
    fn from(c: &C) -> Self {
        Room { min: *c, max: *c }
    }
}

impl<'a, C: 'a + Coord> Extend<&'a C> for Room<C> {
    fn extend<T: IntoIterator<Item = &'a C>>(&mut self, iter: T) {
        for coord in iter {
            self.min = Coord::min(&self.min, coord);
            self.max = Coord::max(&self.max, coord);
        }
    }
}

impl Coord for Coord3 {
    fn neighbors(&self) -> Vec<Self>
    where
        Self: Sized,
    {
        let mut result = Vec::new();
        for x in self.x - 1..=self.x + 1 {
            for y in self.y - 1..=self.y + 1 {
                for z in self.z - 1..=self.z + 1 {
                    result.push(Coord3 { x, y, z });
                }
            }
        }
        result
    }

    fn min(rhs: &Self, lhs: &Self) -> Self {
        Coord3 {
            x: min(rhs.x, lhs.x),
            y: min(rhs.y, lhs.y),
            z: min(rhs.z, lhs.z),
        }
    }

    fn max(rhs: &Self, lhs: &Self) -> Self {
        Coord3 {
            x: max(rhs.x, lhs.x),
            y: max(rhs.y, lhs.y),
            z: max(rhs.z, lhs.z),
        }
    }
}

impl Coord for Coord4 {
    fn neighbors(&self) -> Vec<Self>
    where
        Self: Sized,
    {
        let mut result = Vec::new();
        for x in self.x - 1..=self.x + 1 {
            for y in self.y - 1..=self.y + 1 {
                for z in self.z - 1..=self.z + 1 {
                    for w in self.w - 1..=self.w + 1 {
                        result.push(Coord4 { x, y, z, w });
                    }
                }
            }
        }
        result
    }

    fn min(rhs: &Self, lhs: &Self) -> Self {
        Coord4 {
            x: min(rhs.x, lhs.x),
            y: min(rhs.y, lhs.y),
            z: min(rhs.z, lhs.z),
            w: min(rhs.w, lhs.w),
        }
    }

    fn max(rhs: &Self, lhs: &Self) -> Self {
        Coord4 {
            x: max(rhs.x, lhs.x),
            y: max(rhs.y, lhs.y),
            z: max(rhs.z, lhs.z),
            w: max(rhs.w, lhs.w),
        }
    }
}

impl<C: Coord + Eq + Hash + Copy> Active<C> {
    pub fn count(&self) -> usize {
        self.0.len()
    }

    pub fn room(&self) -> Result<Room<C>, RoomError> {
        let mut result;
        let mut iter = self.0.iter();
        if let Some(first) = iter.next() {
            result = Room::from(first);
        } else {
            return Err(RoomError::EmptyRoom);
        }
        result.extend(iter);
        Ok(result)
    }

    fn count_active_neighbors(&self, c: &C) -> u8 {
        let mut result = 0;
        for neighbor in c.neighbors() {
            if &neighbor != c && self.0.contains(&neighbor) {
                result += 1;
            }
        }
        result
    }

    pub fn power_cycle(&self) -> Active<C> {
        let mut result = HashSet::new();
        for c in self.0.iter() {
            for n in c.neighbors() {
                result.insert(n);
            }
        }
        result.retain(|c| matches!(self.new_state(c), State::Active));
        Active(result)
    }

    fn new_state(&self, c: &C) -> State {
        let active_neighbors = self.count_active_neighbors(c);
        let active = self.0.contains(c);
        if active_neighbors == 3 || (active && active_neighbors == 2) {
            State::Active
        } else {
            State::Inactive
        }
    }

    pub fn power_cycle_nth(self, n: usize) -> Active<C> {
        let mut current = self;
        for _ in 0..n {
            current = current.power_cycle();
        }
        current
    }
}

impl FromStr for Active<Coord3> {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut result = HashSet::new();
        let mut z: isize = 0;
        let mut y: isize = 0;
        for (lineno, line) in s.lines().enumerate() {
            if line.starts_with('z') {
                if let Some(n) = line
                    .split_once("=")
                    .map(|t| t.1)
                    .and_then(|s| s.parse::<isize>().ok())
                {
                    z = n
                } else {
                    return Err(format!("line {}: invalid z coordinate", lineno));
                }
                y = 0;
            } else if !line.is_empty() {
                let active_x = line
                    .chars()
                    .map(State::try_from)
                    .enumerate()
                    .filter_map(|(x, s)| match s {
                        Ok(State::Active) => Some(Ok(x as isize)),
                        Ok(State::Inactive) => None,
                        Err(msg) => Some(Err(format!("line {}: pos {}: {}", lineno, x, msg))),
                    })
                    .collect::<Result<Vec<isize>, String>>()?;
                result.extend(active_x.iter().map(|x| Coord3 { x: *x, y, z }));
                y += 1;
            }
        }
        Ok(Active(result))
    }
}

impl TryFrom<char> for State {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(State::Inactive),
            '#' => Ok(State::Active),
            _ => Err(format!("invalid state: {}", value)),
        }
    }
}

impl Display for State {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            State::Inactive => write!(f, "."),
            State::Active => write!(f, "#"),
        }
    }
}

impl Display for Active<Coord3> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let border = match self.room() {
            Ok(b) => b,
            Err(RoomError::EmptyRoom) => {
                return write!(f, "{}", State::Inactive.to_string());
            }
        };
        for z in border.min.z..=border.max.z {
            writeln!(f, "z={}", z)?;
            for y in border.min.y..=border.max.y {
                for x in border.min.x..=border.max.x {
                    if self.0.contains(&Coord3 { x, y, z }) {
                        write!(f, "{}", State::Active)?;
                    } else {
                        write!(f, "{}", State::Inactive)?;
                    }
                }
                writeln!(f)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl From<&Active<Coord3>> for Active<Coord4> {
    fn from(a: &Active<Coord3>) -> Self {
        let mut result = HashSet::new();
        for c in a.0.iter() {
            result.insert(Coord4 {
                x: c.x,
                y: c.y,
                z: c.z,
                w: 0,
            });
        }
        Active(result)
    }
}

fn input() -> String {
    include_str!("../input/17").to_string()
}

fn calc<C: Coord + Eq + Hash + Copy>(r: Active<C>) -> usize {
    r.power_cycle_nth(6).count()
}

pub fn run() {
    let active3 = input().parse::<Active<Coord3>>().unwrap();
    let active4 = Active::<Coord4>::from(&active3);
    println!("Part 1: {}", calc(active3));
    println!("Part 2: {}", calc(active4));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST: &str = r".#.
..#
###";

    const TEST1: &str = r"z=-1
#..
..#
.#.

z=0
#.#
.##
.#.

z=1
#..
..#
.#.

";

    const TEST2: &str = r"z=-2
.....
.....
..#..
.....
.....

z=-1
..#..
.#..#
....#
.#...
.....

z=0
##...
##...
#....
....#
.###.

z=1
..#..
.#..#
....#
.#...
.....

z=2
.....
.....
..#..
.....
.....

";

    const TEST3: &str = r"z=-2
.......
.......
..##...
..###..
.......
.......
.......

z=-1
..#....
...#...
#......
.....##
.#...#.
..#.#..
...#...

z=0
...#...
.......
#......
.......
.....##
.##.#..
...#...

z=1
..#....
...#...
#......
.....##
.#...#.
..#.#..
...#...

z=2
.......
.......
..##...
..###..
.......
.......
.......

";

    #[test]
    fn test_parse() {
        let a: Active<Coord3> = TEST.parse().unwrap();
        assert_eq!(5, a.count());
    }

    #[test]
    fn test_room() {
        let a: Active<Coord3> = TEST.parse().unwrap();
        let r = a.room().unwrap();
        assert_eq!(0, r.min.x);
        assert_eq!(2, r.max.x);
        assert_eq!(0, r.min.y);
        assert_eq!(2, r.max.y);
        assert_eq!(0, r.min.z);
        assert_eq!(0, r.max.z);
    }

    #[test]
    fn test_display() {
        let a: Active<Coord3> = TEST.parse().unwrap();
        let display = format!("{}", a);
        let expected = format!("z=0\n{}\n\n", TEST);
        assert_eq!(expected, display);
    }

    #[test]
    fn test_power_cycle1() {
        let a: Active<Coord3> = TEST.parse().unwrap();
        assert_eq!(TEST1, format!("{}", a.power_cycle()));
    }

    #[test]
    fn test_power_cycle2() {
        let a: Active<Coord3> = TEST1.parse().unwrap();
        assert_eq!(TEST2, format!("{}", a.power_cycle()));
    }

    #[test]
    fn test_power_cycle3() {
        let a: Active<Coord3> = TEST2.parse().unwrap();
        assert_eq!(TEST3, format!("{}", a.power_cycle()));
    }

    #[test]
    fn test_power_cycle_nth() {
        let a: Active<Coord3> = TEST.parse().unwrap();
        assert_eq!(112, a.power_cycle_nth(6).count());
    }

    #[test]
    fn test_part1() {
        let a = input().parse::<Active<Coord3>>().unwrap();
        assert_eq!(276, calc(a))
    }

    #[test]
    fn test_part2() {
        let a = Active::<Coord4>::from(&input().parse::<Active<Coord3>>().unwrap());
        assert_eq!(2136, calc(a))
    }
}
