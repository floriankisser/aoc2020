struct Numbers {
    two: (usize, usize),
    three: (usize, usize, usize),
}

fn calc() -> Result<Numbers, String> {
    if let Ok(numbers) = include_str!("../input/1")
        .lines()
        .map(|s| s.parse::<usize>())
        .collect::<Result<Vec<_>, _>>()
    {
        let mut result = Numbers {
            two: (0, 0),
            three: (0, 0, 0),
        };
        numbers.iter().enumerate().for_each(|e1| {
            for e2 in (&numbers[e1.0 + 1..]).iter().enumerate() {
                let first = *e1.1;
                let second = *e2.1;
                if first + second == 2020 {
                    result.two = (first, second);
                }
                for third in &numbers[e2.0 + 1..] {
                    if first + second + third == 2020 {
                        result.three = (first, second, *third);
                    }
                }
            }
        });
        Ok(result)
    } else {
        Err(String::from("error while parsing file"))
    }
}

pub fn run() {
    match calc() {
        Ok(numbers) => {
            println!(
                "found numbers {}, {} with result {}",
                numbers.two.0,
                numbers.two.1,
                numbers.two.0 * numbers.two.1,
            );
            println!(
                "found numbers {}, {}, {} with result {}",
                numbers.three.0,
                numbers.three.1,
                numbers.three.2,
                numbers.three.0 * numbers.three.1 * numbers.three.2
            );
        }
        Err(msg) => eprintln!("{}", msg),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc() {
        let result = calc();
        assert!(result.is_ok());

        let numbers = result.unwrap();
        assert_eq!(2020, numbers.two.0 + numbers.two.1);
        assert_eq!(270_144, numbers.two.0 * numbers.two.1);
        assert_eq!(2020, numbers.three.0 + numbers.three.1 + numbers.three.2);
        assert_eq!(
            261_342_720,
            numbers.three.0 * numbers.three.1 * numbers.three.2
        );
    }
}
