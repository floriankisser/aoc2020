use std::collections::HashSet;
use std::str::FromStr;

struct Seat {
    row: u8,
    column: u8,
}

impl Seat {
    pub fn get_id(&self) -> usize {
        self.row as usize * 8 + self.column as usize
    }
}

impl FromStr for Seat {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (row_str, column_str) = s.split_at(7);
        let mut row = 0;
        for c in row_str.chars() {
            row <<= 1;
            if c == 'B' {
                row += 1;
            } else if c != 'F' {
                return Err(format!("unexpected row character: {}", c));
            }
        }
        let mut column = 0;
        for c in column_str.chars() {
            column <<= 1;
            if c == 'R' {
                column += 1;
            } else if c != 'L' {
                return Err(format!("unexpected column character: {}", c));
            }
        }
        Ok(Seat { row, column })
    }
}

fn seats() -> Vec<Seat> {
    include_str!("../input/5")
        .lines()
        .map(Seat::from_str)
        .map(Result::unwrap)
        .collect()
}

fn highest_seat_id(seats: &[Seat]) -> usize {
    seats.iter().map(|s| s.get_id()).max().unwrap_or(0)
}

fn free_seat_id(seats: &[Seat]) -> usize {
    let seat_ids: HashSet<_> = seats.iter().map(|s| s.get_id()).collect();
    let max_id = *seat_ids.iter().max().unwrap();
    for id in (0..max_id).rev() {
        if !seat_ids.contains(&id) && seat_ids.contains(&(id + 1)) && seat_ids.contains(&(id - 1)) {
            return id;
        }
    }
    0
}

pub fn run() {
    println!("highest seat ID is {}", highest_seat_id(&seats()));
    println!("free seat ID is {}", free_seat_id(&seats()));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_highest_seat_id() {
        assert_eq!(885, highest_seat_id(&seats()));
    }

    #[test]
    fn test_free_seat_id() {
        assert_eq!(623, free_seat_id(&seats()));
    }

    #[test]
    fn test_seat_from_str_1() {
        let seat = Seat::from_str(r"BFFFBBFRRR").unwrap();
        assert_eq!(70, seat.row);
        assert_eq!(7, seat.column);
        assert_eq!(567, seat.get_id());
    }

    #[test]
    fn test_seat_from_str_2() {
        let seat = Seat::from_str(r"FFFBBBFRRR").unwrap();
        assert_eq!(14, seat.row);
        assert_eq!(7, seat.column);
        assert_eq!(119, seat.get_id());
    }

    #[test]
    fn test_seat_from_str_3() {
        let seat = Seat::from_str(r"BBFFBBFRLL").unwrap();
        assert_eq!(102, seat.row);
        assert_eq!(4, seat.column);
        assert_eq!(820, seat.get_id());
    }
}
