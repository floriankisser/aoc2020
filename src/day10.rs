use std::collections::HashMap;

fn input() -> String {
    include_str!("../input/10").to_string()
}

fn parse(s: &str) -> Vec<usize> {
    let mut input: Vec<usize> = s.lines().map(|l| l.parse().unwrap()).collect();
    input.push(input.iter().max().unwrap() + 3);
    input.sort_unstable();
    input
}

fn distances(i: &[usize]) -> Vec<usize> {
    i.iter()
        .scan(0, |p, &e| {
            let d = e - *p;
            if d > 3 {
                panic!("distance greater than 3")
            }
            *p = e;
            Some(d)
        })
        .collect()
}

fn distribution(distances: &[usize]) -> (usize, usize, usize) {
    let mut distribution = HashMap::new();
    for d in distances {
        *distribution.entry(d).or_insert(0usize) += 1;
    }
    (
        *distribution.entry(&1).or_insert(0usize),
        *distribution.entry(&2).or_insert(0usize),
        *distribution.entry(&3).or_insert(0usize),
    )
}

fn calculate_paths(distances: &[usize]) -> usize {
    distances
        .iter()
        .scan(0, |state, &e| match e {
            1 => {
                *state += 1;
                Some(1)
            }
            3 => {
                let f = if *state < 2 {
                    1
                } else if *state == 2 {
                    2
                } else if *state == 3 {
                    4
                } else if *state == 4 {
                    7
                } else {
                    panic!("unexpected state")
                };
                *state = 0;
                Some(f)
            }
            _ => panic!("unexpected distance"),
        })
        .product()
}

pub fn run() {
    let distances = distances(&parse(&input()));
    let distribution = distribution(&distances);
    println!(
        "distance counts: {}x1 {}x2 {}x3; count 1 x count 3: {}",
        distribution.0,
        distribution.1,
        distribution.2,
        distribution.0 * distribution.2
    );
    println!("number of combinations: {}", calculate_paths(&distances));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let distribution = distribution(&distances(&parse(&input())));
        assert_eq!(2080, distribution.0 * distribution.2);
    }

    #[test]
    fn test_part2() {
        assert_eq!(
            6_908_379_398_144,
            calculate_paths(&distances(&parse(&input())))
        );
    }
}
