use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
struct Password {
    policy: Policy,
    value: String,
}

#[derive(Debug)]
struct Policy {
    min: usize,
    max: usize,
    character: char,
}

impl FromStr for Password {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) (\pL): (\pL+)$").unwrap();
        }
        if let Some(captures) = RE.captures(s) {
            Ok(Password {
                policy: Policy {
                    min: captures[1].parse::<usize>().unwrap(),
                    max: captures[2].parse::<usize>().unwrap(),
                    character: captures[3].chars().next().unwrap(),
                },
                value: captures[4].to_string(),
            })
        } else {
            Err(format!("cannot parse password: {}", s))
        }
    }
}

fn check_old_policy(password: &Password) -> bool {
    let count = password
        .value
        .chars()
        .filter(|c| *c == password.policy.character)
        .count();
    count >= password.policy.min && count <= password.policy.max
}

fn check_new_policy(p: &Password) -> bool {
    check_nth_char(&p.value, &p.policy.min - 1, &p.policy.character)
        ^ check_nth_char(&p.value, &p.policy.max - 1, &p.policy.character)
}

fn check_nth_char(value: &str, n: usize, char: &char) -> bool {
    value.chars().nth(n).filter(|c| c == char).is_some()
}

fn calc() -> (usize, usize) {
    let mut count_old = 0;
    let mut count_new = 0;
    for password in include_str!("../input/2")
        .lines()
        .map(|s| s.parse::<Password>())
    {
        match password {
            Ok(password) => {
                if check_old_policy(&password) {
                    count_old += 1;
                }
                if check_new_policy(&password) {
                    count_new += 1;
                }
            }
            Err(error) => eprintln!("{}", error),
        }
    }
    (count_old, count_new)
}

pub fn run() {
    let result = calc();
    println!("found {} passwords valid for old policy", result.0);
    println!("found {} passwords valid for new policy", result.1);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc() {
        let result = calc();
        assert_eq!(591, result.0);
        assert_eq!(335, result.1);
    }
}
