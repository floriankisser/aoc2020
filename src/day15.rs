use std::collections::HashMap;
use std::iter::once;
use std::str::FromStr;

fn input() -> String {
    String::from("20,9,11,0,1,2")
}

fn parse(s: &str) -> HashMap<usize, usize> {
    once(0)
        .chain(s.split(',').map(usize::from_str).map(Result::unwrap))
        .enumerate()
        .map(|(a, b)| (b, a))
        .skip(1)
        .collect()
}

fn calc(map: &mut HashMap<usize, usize>, at: usize) -> usize {
    let mut current = 0;
    for pos in map.len() + 1..at {
        current = match map.get_mut(&current) {
            None => {
                map.insert(current, pos);
                0
            }
            Some(last) => {
                let diff = pos - *last;
                *last = pos;
                diff
            }
        }
    }
    current
}

pub fn run() {
    let start = parse(&input());
    println!("Part 1: {}", calc(&mut start.clone(), 2020));
    let mut start = start;
    println!("Part 2: {}", calc(&mut start, 30_000_000));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let start = parse(&input());
        assert_eq!(6, start.len());
        assert_eq!(Some(&1), start.get(&20));
        assert_eq!(Some(&2), start.get(&9));
        assert_eq!(Some(&3), start.get(&11));
        assert_eq!(Some(&4), start.get(&0));
        assert_eq!(Some(&5), start.get(&1));
        assert_eq!(Some(&6), start.get(&2));
    }

    #[test]
    fn test_part1a() {
        assert_eq!(436, calc(&mut parse("0,3,6"), 2020));
    }

    #[test]
    fn test_part1() {
        assert_eq!(1111, calc(&mut parse(&input()), 2020));
    }

    #[test]
    fn test_part2() {
        assert_eq!(48568, calc(&mut parse(&input()), 30_000_000));
    }
}
