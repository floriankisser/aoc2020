use lazy_static::lazy_static;

pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day2;
pub mod day20;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

lazy_static! {
    pub static ref DAYS: Vec<(String, fn())> = vec![
        (String::from("1"), day1::run),
        (String::from("2"), day2::run),
        (String::from("3"), day3::run),
        (String::from("4"), day4::run),
        (String::from("5"), day5::run),
        (String::from("6"), day6::run),
        (String::from("7"), day7::run),
        (String::from("8"), day8::run),
        (String::from("9"), day9::run),
        (String::from("10"), day10::run),
        (String::from("11"), day11::run),
        (String::from("12"), day12::run),
        (String::from("13"), day13::run),
        (String::from("14"), day14::run),
        (String::from("15"), day15::run),
        (String::from("16"), day16::run),
        (String::from("17"), day17::run),
        (String::from("18"), day18::run),
        (String::from("19"), day19::run),
        (String::from("20"), day20::run),
    ];
}
