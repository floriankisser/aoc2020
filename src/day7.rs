use std::collections::HashSet;
use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;

struct Rule {
    color: String,
    bags: Vec<(usize, String)>,
}

impl FromStr for Rule {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(?:(\d+) )?(\w+ \w+) bags?").unwrap();
        }
        let mut bags: Vec<(usize, String)> = Vec::new();
        let mut color: Option<String> = None;
        for capture in RE.captures_iter(s) {
            let bag_color = capture[2].to_string();
            if let Some(count) = capture.get(1) {
                bags.push((count.as_str().parse::<usize>().unwrap(), bag_color));
            } else if bag_color != "no other" {
                color = Some(bag_color);
            }
        }
        Ok(Rule {
            color: color.unwrap(),
            bags,
        })
    }
}

fn input() -> String {
    include_str!("../input/7").to_string()
}

fn rules(s: &str) -> Vec<Rule> {
    s.lines().map(Rule::from_str).map(Result::unwrap).collect()
}

fn rules_for_color(rules: &[Rule], color: &str) -> HashSet<String> {
    let mut colors: HashSet<String> = HashSet::new();
    for rule in rules.iter() {
        for bag in rule.bags.iter() {
            if bag.1 == color && !colors.contains(&rule.color) {
                colors.insert(rule.color.clone());
                colors = colors
                    .union(&rules_for_color(rules, &rule.color))
                    .cloned()
                    .collect();
            }
        }
    }
    colors
}

fn bags_for_color(rules: &[Rule], color: &str) -> usize {
    let mut count = 0;
    if let Some(rule) = rules.iter().find(|r| r.color == color) {
        for bag in rule.bags.iter() {
            count += bag.0 + bag.0 * bags_for_color(rules, &bag.1);
        }
    } else {
        panic!("no rule found for color: {}", color);
    }
    count
}

pub fn run() {
    let rules = rules(&input());
    println!(
        "found {} bag colors",
        rules_for_color(&rules, "shiny gold").len()
    );
    println!("calculated {} bags", bags_for_color(&rules, "shiny gold"));
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";

    #[test]
    fn test_rules() {
        assert_eq!(594, rules(&input()).len())
    }

    #[test]
    fn test_rule1() {
        let rule: Rule = "striped olive bags contain 4 dark crimson bags."
            .parse()
            .unwrap();
        assert_eq!("striped olive", rule.color);
        assert_eq!(1, rule.bags.len());
        assert_eq!(4, rule.bags[0].0);
        assert_eq!("dark crimson", rule.bags[0].1);
    }

    #[test]
    fn test_rule2() {
        let rule: Rule = "dotted indigo bags contain 4 faded black bags, 4 clear cyan bags, 5 vibrant teal bags."
            .parse()
            .unwrap();
        assert_eq!("dotted indigo", rule.color);
        assert_eq!(3, rule.bags.len());
        assert_eq!(4, rule.bags[0].0);
        assert_eq!("faded black", rule.bags[0].1);
        assert_eq!(4, rule.bags[1].0);
        assert_eq!("clear cyan", rule.bags[1].1);
        assert_eq!(5, rule.bags[2].0);
        assert_eq!("vibrant teal", rule.bags[2].1);
    }

    #[test]
    fn test_rule3() {
        let rule: Rule = "bright white bags contain 1 shiny gold bag."
            .parse()
            .unwrap();
        assert_eq!("bright white", rule.color);
        assert_eq!(1, rule.bags.len());
        assert_eq!(1, rule.bags[0].0);
        assert_eq!("shiny gold", rule.bags[0].1);
    }

    #[test]
    fn test_rule4() {
        let rule: Rule = "pale tan bags contain no other bags.".parse().unwrap();
        assert_eq!("pale tan", rule.color);
        assert!(rule.bags.is_empty());
    }

    #[test]
    fn test_part1a() {
        assert_eq!(4, rules_for_color(&rules(INPUT), "shiny gold").len());
    }

    #[test]
    fn test_part1() {
        assert_eq!(257, rules_for_color(&rules(&input()), "shiny gold").len());
    }

    #[test]
    fn test_part2a() {
        assert_eq!(32, bags_for_color(&rules(INPUT), "shiny gold"));
    }

    #[test]
    fn test_part2b() {
        let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        assert_eq!(126, bags_for_color(&rules(input), "shiny gold"));
    }

    #[test]
    fn test_part2() {
        assert_eq!(1038, bags_for_color(&rules(&input()), "shiny gold"));
    }
}
