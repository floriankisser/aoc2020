use std::collections::HashSet;

fn groups() -> Vec<Vec<HashSet<char>>> {
    let mut groups = Vec::new();
    for group in include_str!("../input/6").split("\n\n") {
        groups.push(group.lines().map(|l| l.chars().collect()).collect());
    }
    groups
}

fn union<'a, I>(group: I) -> HashSet<char>
where
    I: IntoIterator<Item = &'a HashSet<char>>,
{
    group
        .into_iter()
        .fold(HashSet::new(), |set, o| set.union(o).copied().collect())
}

fn intersection<'a, I>(group: I) -> HashSet<char>
where
    I: IntoIterator<Item = &'a HashSet<char>>,
{
    let mut iter = group.into_iter();
    let mut answers = iter.next().unwrap().clone();
    for e in iter {
        answers = answers.intersection(e).copied().collect();
    }
    answers
}

fn answer1() -> usize {
    groups().iter().map(|g| union(g).len()).sum()
}

fn answer2() -> usize {
    groups().iter().map(|g| intersection(g).len()).sum()
}

pub fn run() {
    println!("found {} yes answers", answer1());
    println!("found {} all yes answers", answer2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_groups() {
        assert_eq!(454, groups().len());
    }

    #[test]
    fn test_count_answers() {
        assert_eq!(6335, answer1());
    }

    #[test]
    fn test_count_answers2() {
        assert_eq!(3392, answer2());
    }
}
