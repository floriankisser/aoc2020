#!/usr/bin/env sh

set -ex
. ./env.sh

base=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/base
build=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-rust

build_ctr=$(buildah from -v "$(pwd):/workspace:z" "$build")
buildah config --workingdir /workspace "$build_ctr"

buildah run "$build_ctr" -- cargo build --release

buildah rm "$build_ctr"

ctr=$(buildah from "$base")

buildah add "$ctr" target/release/aoc2020 /usr/local/bin/

buildah config \
  --label "name=aoc2020" \
  --label "commit=$CI_COMMIT_SHA" \
  --entrypoint '["/usr/local/bin/aoc2020"]' \
  "$ctr"

buildah commit --rm "$ctr" aoc2020
